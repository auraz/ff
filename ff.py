#!/usr/bin/python
# -*- coding: utf-8 -*-
# run interface
from interface import *

if __name__ == '__main__':
    app = QApplication(sys.argv)
    frame = MainWindow()
    frame.show()   
    app.exec_()
