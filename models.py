﻿# -*- coding: utf-8 -*-
# alexander kryklia
# kryklia@gmail.com
# 2011-2012
# todo
# Сейчас продукты в списке сортируются по дате покупки, а можно сортировать по дате добавления,например, если вводятся покупки за позавчера, а хочется проверять ввод  сейчас же
# где поместить кнопку для пересчета суммы.

CONST_UNKNOWN = ' '


import pickle, datetime, gc
from operator import attrgetter

def canon_name(name):
    return unicode(name).lower()


def log(a):
    pass
    #print a
    
class Category(object):
    """
    Storage for all categories
    """
     
        
    def __init__(self, name = None):
        super(Category, self).__init__()
        self.prod2cat ={}
        
        if name:
            name = canon_name(name)
            self.__dict__[name] = []

    def __str__(self):
        return unicode(self.__dict__)

    def exist(self, name):
        ''' this category [name] exist? '''
        return True if self.__dict__.get(canon_name(name), False) != False else False
        
    def add(self, name):
        name = canon_name(name)
        self.__dict__[name] = []

    def remove(self, name):
        name = canon_name(name)
        try:
            del  self.__dict__[name]
        except:
            log("no such category", name)
        try:
            print self.__dict__[name]
        except:
            log("Category", name, "deleted")
        
    def remove_product_from_category(self, name, product_name):
        name = canon_name(name)
        product_name = canon_name(product_name)
        try:
            self.__dict__[name].remove(product_name)
        except:
            log("no product", product_name)
        del self.prod2cat[product_name]
            
    def add_prod_to_cat(self, name, product_name):
        name = canon_name(name)
        product_name = canon_name(product_name)
        oldname =  self.prod2cat.get(product_name, [])
        if oldname and oldname != name:
            self.__dict__[oldname].remove(product_name)
            
        if self.exist(name):
            if product_name not in self.__dict__[name]:
                self.__dict__[name].append(product_name)
        else:
            self.__dict__[name]=[product_name]

        self.prod2cat[product_name] = name       
            
        
    def get_cat_by_prod(self, product_name):
        ''' return One category for product'''
        return self.prod2cat.get(canon_name(product_name), [])
        
    def get_prods_by_cat(self, catname):
        return self.__dict__.get(canon_name(catname), None)
        
    def change_cat_of_prod(self, prodname, catname):
        prodname = canon_name(prodname)
        catname = canon_name(catname)
        oldcat = self.get_cat_by_prod(prodname)
        if oldcat:
            self.remove_product_from_category(oldcat, prodname)
            self.add_prod_to_cat(catname, prodname)
        else:
            log('prodname', 'not in categoryes at all, adding')
            self.add_prod_to_cat(catname, prodname)
    
    def addprod(self, prodname):
        prodname = canon_name(prodname)
        if self.get_cat_by_prod(prodname):
            pass
        else:
            self.add_prod_to_cat(CONST_UNKNOWN, prodname) 

    

        
        
        
class Data(Category):
    def __init__(self, name = None):
        super(Data, self).__init__()
        self.sp = []# spisok pokupok
        self.len_sp = 0
        self.sum = 0
            
            
    def update_sum(self, sum):        
        self.sum +=sum
    
    
    def generate_print_output(self, date_start=None, date_end=None):
        ''' generate string for print'''
        s=[]
        if date_start == None:  # export for today
            for i in xrange(0, self.len_sp):
                if self.sp[i].when_added.date() == datetime.datetime.today().date():
                    s.append((self.sp[i].prodname, self.sp[i].price, self.get_cat_by_prod(self.sp[i].prodname)))
                else:
                    break
            #print s    
        else:
            for i in xrange(0, self.len_sp):
                if self.sp[i].when_added.date() >= date_start and self.sp[i].when_added.date() <= date_end:
                    s.append((self.sp[i].prodname, self.sp[i].price, self.sp[i].when_added.strftime("%d/%m/%y"), self.get_cat_by_prod(self.sp[i].prodname)))
                elif self.sp[i].when_added.date() > date_end:
                    break
            #print s
        return s
        
    def get_sum_for_today(self):  
        ''' slow, refactor '''
        sum_today = 0
        for i in xrange(0, self.len_sp):
            if self.sp[i].when_added.date() == datetime.datetime.today().date():
                sum_today += self.sp[i].price
            else:
                break  # list is sorted from closest to farest dates
        return sum_today
        
    def remove_pokupka(self, index):
        ''' remove pokupka from list '''
        self.sum -= self.sp[index].price
        del self.sp[index]
        self.len_sp -= 1
    
class Pokupka(object):
    """docstring for Product"""

    
    def __init__(self, name, Data_object, price, when=None, category_name = None):
        super(Pokupka, self).__init__()
        self.prodname = canon_name(name)
        if category_name:
            Data_object.add_prod_to_cat(category_name, self.prodname)
        elif Data_object.get_cat_by_prod(self.prodname):
            pass
        else:
            Data_object.add_prod_to_cat(CONST_UNKNOWN, self.prodname)
        
        if not when:
            self.when_added = datetime.datetime.now() #time not date, time is really needed for sort
        else:
            t = datetime.datetime.now()
            if when.year < 100:
                when.year +=2000
            self.when_added = datetime.datetime(when.year, when.month, when.day, t.hour, t.minute, t.second)
            
            #datetime.datetime.combine(when, datetime.time()) # zero time here
        self.price = price
        
        Data_object.sp.append(self)
        
        Data_object.sp.sort(key=attrgetter('when_added'), reverse = True)
        
        Data_object.len_sp +=1
        
        Data_object.update_sum(self.price)
        
    def __str__(self):
        return self.prodname +' '+ unicode(self.price) +  '  ' + self.when_added.strftime("%d/%m/%y") 

    def __unicode__(self):
        return self.prodname +' '+ unicode(self.price) +  '  ' + self.when_added.strftime("%d/%m/%y") 

        
def out_data(data):
    '''data is datetime.datetine.now()'''
    delta = abs(data.date()-datetime.date.today()).days
    if delta == 0:
        out = u'Сегодня'
    elif delta == 1:
        out = u'Вчера'
    elif delta == 2:
        out = u'Позавчера'
    elif delta < 7:
        out = unicode(delta) + u' дня назад'
    else:
        out = data.strftime("%d/%m/%y")
    return out
    
def getPokupki(d, index, n=5):
    '''
    return a list of n pokupkas from index
    Example: if index = 1 ,then return 1-n pokupki, if index = 5 then retrun [5,6,7..n] pokupki
    index counts from 0
    n - number of pokupok
    '''
    assert index >= 0
    assert index + n <= len(d.sp)
    if d.sp:
        output = []
        for i in xrange(index, index + n):
            x=d.sp[i]
            output.append((x.prodname, unicode(x.price), out_data(x.when_added) , d.get_cat_by_prod(x.prodname), unicode(i+1)))
        return output
    else:
        return []
        
        
class Controller(object):
    """docstring for Controller"""

    finance_filename = 'finances.db'

    def __init__(self):
        super(Controller, self).__init__()

    def rename_product_v_pokupke(self):
        pass

    def rename_category(self):
        pass

    def save(self, data):
        log("saving")
        f = open(self.finance_filename, 'wb')
        to_dump = data
        pickle.dump(to_dump,f)
        f.close()

    def load(self):
        status = False
        try:
            f = open(self.finance_filename, 'rb')
        except IOError:
            # new file will be created
            log('Nothing to load, creating new')
        else:
            log("Loading")
            status = True
        
        if status == True:
            try:
                data = pickle.load(f)
                f.close()
            except EOFError:
                log("File ", finance_filename, "is corrupted.")
        else:
            data = Data()
            
        return data
            
def test_cats():
    #test category
    global c
    c = Data()
    print c
    c.add('1')
    print c.exist('1')
    c.add('2')
    print c
    c.remove('3')
    print c
    c.add_prod_to_cat('1','p1')
    print c
    c.add_prod_to_cat('1','p2')
    print c
    c.remove_product_from_category('1', 'p2')
    print c
    print c.prod2cat
    c.get_cat_by_prod('p1')
    print c
    print c.get_prods_by_cat('2')
    print c
    # test product
    p = Pokupka('ogurec', c, 10, None, 'ovosh')
    print p
    print c
    p = Pokupka('ogurec', c, 10)
    print p
    print c
    p = Pokupka('pomidor', c, 10)
    print p
    print c
    c.change_cat_of_prod('pomidor', 'ovosh')
    print c
    c.change_cat_of_prod('potato', 'ovosh')
    print c
    c.addprod('banan')
    print c
    c.change_cat_of_prod('banan', 'frukt')
    print c
    print c.sp
    global sp
    sp = c.sp
    print "---------"
    
def test_contr():    
    # test controller
    global c, sp
    k = Controller()
    print c, sp
    k.save(c)
    c={}
    sp={}
    print c, sp
    c = k.load()
    print c, c.sp
    

if __name__ == "__main__":
    test_cats()
    test_contr()