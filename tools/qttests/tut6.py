#!/usr/bin/env python
# licence.py - display GPL licence
 
import sys
 
from PySide.QtGui import QApplication, QMainWindow, QTextEdit, QPushButton
 
from ui_licence import Ui_MainWindow
 
class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        '''Mandatory initialisation of a class.'''
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.showButton.clicked.connect(self.fileRead)
         
    def fileRead(self):
        '''Read and display GPL licence.'''
        self.textEdit.setText(open('COPYING.txt').read())
         
if __name__ == '__main__':
    app = QApplication(sys.argv)
    frame = MainWindow()
    frame.show()
    app.exec_()