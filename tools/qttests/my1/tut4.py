#!/usr/bin/env python
# quitter.py - provide a button to quit this "program"
 
import sys
from PySide import QtGui, QtCore
from PySide.QtGui import QMainWindow, QPushButton, QApplication, QRegion, QPixmap
from PySide.QtCore import QRect, QSize
from add1 import Ui_MainWindow

import random



    


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        
        
    ##def resizeEvent(self, event):
        #self.setMask(maskedRegion)
        ###maskedRegion=self.mask2()
        ###self.setMask(maskedRegion)
        #self.setWindowFlags(QtCore.Qt.FramelessWindowHint)  # eto bilo vklucheno i norm
        #self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        #self.setStyleSheet("background:transparent;")
        self.setStyleSheet("background-color:#f3f3f3;")
        #self.setStyleSheet("border-style:solid;")
        #self.setStyleSheet("border:5px;")
        #self.setStyleSheet("border-color:#6952a3;")    
        
    def resizeEvent(self, event):
        size = self.size
        pixmap =  QtGui.QPixmap()
        painter=  QtGui.QPainter(pixmap);
        painter.begin(self)
        painter.fillRect(pixmap.rect(), QtCore.Qt.white);
        painter.setBrush(QtCore.Qt.black);
        painter.drawRoundRect(pixmap.rect());
        mask= pixmap.createMaskFromColor(QtCore.Qt.white)
        painter.end()
        self.setMask(mask)
        
    
    #def paintEvent(self, e):

    #    qp = QtGui.QPainter()
    #    qp.begin(self)
        #self.drawPoints(qp)
        #self.drawLines(qp)
    #    self.drawBorder(qp)
    #    qp.end()
        
    def drawPoints(self, qp):
      
        qp.setPen(QtCore.Qt.red)
        size = self.size()
        
        for i in range(1000):
            x = random.randint(1, size.width()-1)
            y = random.randint(1, size.height()-1)
            qp.drawPoint(x, y)     
    
    def drawLines(self, qp):
        r=33.
        dx=640.
        dy=340.
        pen = QtGui.QPen(QtGui.QColor(105, 82, 163), 3, QtCore.Qt.SolidLine)
        
        qp.setPen(pen)
        qp.drawLine(0, r/2,0, dy-r/2)
        qp.drawLine(r/2, dy,dx-r/2, dy)
        qp.drawLine(dx, dy-r/2,dx, r/2)
        qp.drawLine(dx-r/2, 0, r/2, 0)
        

    def drawBorder(self,qp):
        r=17.
        dx=640.
        dy=340.
        x=1.
        y=3#penwidth
        rect = QRect(x,x,dx-2*x,dy-2*x)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(105, 82, 163), y, QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.drawRoundedRect(rect, r, r, QtCore.Qt.AbsoluteSize)
        #x=QtGui.QPainter.HighQualityAntialiasing
        #QtGui.QPainter.setRenderHint()

            
    def mask1(self):
        r=40.
        dx=640.
        dy=340.
        region = QRegion()
        rect = QRect()
        # middle and borders
        region += rect.adjusted(0, r/2,dx, dy-r/2);
        region += rect.adjusted(r/2, 0, dx-r/2, dy);
        rect = rect.adjusted(0,0,dx,dy)
        # top left
        corner = QRect(rect.topLeft(), QSize(r, r));
        region += QRegion(corner, QRegion.Ellipse);
        # top right
        corner.moveTopRight(rect.topRight());
        region += QRegion(corner, QRegion.Ellipse);
        # bottom left
        corner.moveBottomLeft(rect.bottomLeft());
        region += QRegion(corner, QRegion.Ellipse);
        # bottom right
        corner.moveBottomRight(rect.bottomRight());
        region += QRegion(corner, QRegion.Ellipse);
        return region;
        
    def mask2(self):
        r=40.
        dx=640.
        dy=340.
        size = self.size
        #print size
        pixmap =  QtGui.QPixmap()
        #print pm, type(pm)
        painter=  QtGui.QPainter(pixmap);
        painter.begin(self)
        painter.fillRect(pixmap.rect(), QtCore.Qt.white);
        painter.setBrush(QtCore.Qt.black);
        painter.drawRoundRect(pixmap.rect());
        painter.end()
        #setMask(pixmap.createMaskFromColor(QtCore.Qt.white));
        return pixmap.createMaskFromColor(QtCore.Qt.white)
        
        
    def mousePressEvent(self, event):
        self.offset = event.pos()

    def mouseMoveEvent(self, event):
        x=event.globalX()
        y=event.globalY()
        x_w = self.offset.x()
        y_w = self.offset.y()
        self.move(x-x_w, y-y_w)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    frame = MainWindow()

    frame.show()   
    app.exec_()