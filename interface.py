﻿#!/usr/bin/python
# -*- coding: utf-8 -*-
# alexander kryklia
# kryklia@gmail.com
# 2011-2012
# family finances interface module
import sys, os
from models import *
import random

from PySide import QtGui, QtCore
from PySide.QtGui import QMainWindow, QPushButton, QApplication, QRegion, QPixmap
from PySide.QtCore import QRect, QSize

# pyside-uic mainwindow.ui  > qtmainwindow.py
from qtmainwindow import Ui_MainWindow

border_color = (105, 82, 163)
bckg_color = (244, 243, 243)

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        # rounded corners of main window
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setupUi(self)
        
        # set icon
        iconpath = './icons/ff.png'
        self.setWindowIcon(QtGui.QIcon(iconpath))
        
        # hide to sys tray
        traySignal = "activated(QSystemTrayIcon::ActivationReason)"
        self.sysTray = QtGui.QSystemTrayIcon(self)
        self.sysTray.setIcon( QtGui.QIcon(iconpath) )
        QtCore.QObject.connect(self.sysTray, QtCore.SIGNAL(traySignal), self.__icon_activated)

        # load data 
        self.controller = Controller()
        self.data = self.controller.load()

        # index of window line to show product
        self.index = 0
        
        # set initial dates to today
        year, month, day = datetime.datetime.today().year, datetime.datetime.today().month, datetime.datetime.today().day
        qdate = QtCore.QDate(year, month, day)
        self.date_in.setDate(qdate)  # date to today
        self.date_from.setDate(qdate)
        self.date_to.setDate(qdate)
        
        # print products
        self.update_output()
        #print self.data.sp, self.data.len_sp
        
        # function
        self.AddButton.clicked.connect(self.add)
        self.UpButton.clicked.connect(self.up)
        self.DownButton.clicked.connect(self.down)
        self.DelButton.clicked.connect(self.remove)
        
        self.pdf_today.clicked.connect(self.export_today)
        self.pdf_range.clicked.connect(self.export_range)
        
        self.printer = QtGui.QPrinter()
        self.printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        self.printer.setPageSize(QtGui.QPrinter.A4)

    def paintEvent(self, e):
        ''' border of main window - event handler '''
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawBorder(qp)
        qp.end()

    def drawBorder(self,qp):
        ''' border of main window '''
        r=17.
        dx=640.
        dy=340.
        x=1.
        y=3#penwidth
        rect = QRect(x,x,dx-2*x,dy-2*x)
        qp.setRenderHint(QtGui.QPainter.Antialiasing)
        pen = QtGui.QPen(QtGui.QColor(*border_color), y, QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtGui.QColor(*bckg_color))
        qp.drawRoundedRect(rect, r, r, QtCore.Qt.AbsoluteSize)
    
    def clear_input(self):
        ''' clear input fields '''
        self.name_in.setText('')
        self.price_in.setText('')
        # date_in do not change
        self.cat_in.setText('')
    
    def print_output(self,y):
        ''' output to table of products '''
        assert y
        assert len(y) == 5
        
        for i in xrange(0,len(y)):
            self.findChild(QtGui.QLabel, "outName" + unicode(i+1)).setText(y[i][0])
            self.findChild(QtGui.QLabel, "outPrice" + unicode(i+1)).setText(y[i][1])
            self.findChild(QtGui.QLabel, "outDate" + unicode(i+1)).setText(y[i][2])
            self.findChild(QtGui.QLabel, "outCat" + unicode(i+1)).setText(y[i][3])   
            
            self.findChild(QtGui.QLabel, "num" + unicode(i+1)).setText(y[i][4])
        
            
    def update_output(self):
        ''' update output with new product '''
        if self.index < 0:
            y= []
            for i in xrange(self.index, 0):
                y.append((' ', ' ', ' ', ' ', ' '))
            y.extend(getPokupki(self.data, 0, min(5+self.index, self.data.len_sp)))
            if self.data.len_sp < 5:
                for i in range(0, 5 - self.data.len_sp + self.index):
                    y.append((' ', ' ', ' ', ' ', ' '))
        elif self.index > self.data.len_sp-5:
            delta = 5 - (self.data.len_sp - self.index)
            y = getPokupki(self.data, self.index, 5 - delta)
            for i in xrange(0, delta):
                y.append((' ', ' ', ' ', ' ', ' '))
        else: 
            y = getPokupki(self.data, self.index, 5)
                
        self.print_output(y)
        self.sum_today.setText(unicode(self.data.get_sum_for_today()))
        self.sum_total.setText(unicode(self.data.sum))
    

    def keyPressEvent(self, e):
        ''' key press but not inside the qlineedit '''
        self.unhighlight_error('product')
        self.unhighlight_error('price')
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()
    
    def export_today(self):
        """
        Export to a pdf file data for today
        """
        filename = datetime.datetime.today().date().__str__() + '.pdf'
        self.printer.setOutputFileName(filename)
        assert self.printer.isValid() == True
        painter = QtGui.QPainter(self.printer)
        to_print = self.data.generate_print_output()
        painter.drawText(300, 50, 'Today')
        painter.drawText(100, 80, 'product')
        painter.drawText(300, 80, 'price')
        painter.drawText(500, 80, 'category')
        i = 0
        for index, line in enumerate(to_print):
            painter.drawText(50, 120 + index * 30, str(index + 1) + '.') 
            painter.drawText(100, 120 + index * 30, line[0])
            painter.drawText(300, 120 + index * 30, str(line[1]))
            painter.drawText(500, 120 + index * 30, line[2])
            i = index
        painter.drawText(100, 150 + i * 30, u'Всего:')
        painter.drawText(300, 150 + i * 30, unicode(self.data.get_sum_for_today()))
        painter.end()

        
    def export_range(self):
        """
        Export to a pdf file data for range of dates
        """
        Start_Date = self.date_from.dateTime().toPython().date()
        End_Date = self.date_to.dateTime().toPython().date()
        filename = self.date_from.text().replace('.','-') + '_' + self.date_to.text().replace('.', '-') + '.pdf'
        #import ipdb; ipdb.set_trace()
        self.printer.setOutputFileName(filename)
        assert self.printer.isValid() == True
        painter = QtGui.QPainter(self.printer)
        to_print = self.data.generate_print_output(date_start=Start_Date, date_end=End_Date)
        painter.drawText(300, 50, 'Finances')
        painter.drawText(100, 80, 'product')
        painter.drawText(300, 80, 'price')
        painter.drawText(500, 80, 'date')
        painter.drawText(700, 80, 'category')
        i = 0
        sum = 0
        for index, line in enumerate(to_print):
            painter.drawText(50, 120 + index * 30, str(index + 1) + '.') 
            painter.drawText(100, 120 + index * 30, line[0])
            painter.drawText(300, 120 + index * 30, str(line[1]))
            painter.drawText(500, 120 + index * 30, line[2])
            painter.drawText(700, 120 + index * 30, line[3])
            i = index
            sum +=line[1]
        painter.drawText(100, 150 + i * 30, u'Всего:')
        painter.drawText(300, 150 + i * 30, unicode(sum))
        painter.end()

    def mousePressEvent(self, event):
        ''' drag and drop '''
        self.offset = event.pos()

    def mouseMoveEvent(self, event):
        ''' drag and drop '''
        x=event.globalX()
        y=event.globalY()
        x_w = self.offset.x()
        y_w = self.offset.y()
        self.move(x-x_w, y-y_w)
    
    def up(self):
        if self.index > -1:
            self.index -= 1
        self.update_output()
    
    def down(self):
        if self.index < self.data.len_sp-2 or self.index < 0:
            self.index+=1
        self.update_output()
        
    def remove(self):
        ''' delete row in table, index + 1 '''
        self.data.remove_pokupka(self.index + 1)
        self.controller.save(self.data)
        self.update_output()
   
    def highlight_error(self, QLabelName):
        ''' Cnahge color of label to highlight label's name as error'''
        self.findChild(QtGui.QLabel, QLabelName).setText("<font color='red'>" + QLabelName + "</font>")

    def unhighlight_error(self, QLabelName):
        ''' Cnahge color of label to highlight label's back to default'''
        self.findChild(QtGui.QLabel, QLabelName).setText("<font color='black'>" + QLabelName + "</font>")
        
    def show_error(self, error):
        ''' show error in error place - not used '''
        print 'keypressed'
        self.error.setText(error)
        
    def parse_data(self,s): 
        ''' transform string to python date '''
        return datetime.date(int(s[6:10]), int(s[3:5]), int(s[0:2]))

    def add(self):

        try:
            args=[]
            #import ipdb; ipdb.set_trace()
            args.append(self.name_in.text())
            args.append(self.data)
            args.append(self.price_in.text())
            args.append(unicode(self.date_in.text()))
            args.append(self.cat_in.text())
            if not args[0]:
                self.highlight_error('product')
                self.unhighlight_error('price')
                return
            elif not args[2]:
                self.highlight_error('price')
                self.unhighlight_error('product')
                return
            args[0]=unicode(args[0])
            args[2]=float(args[2].replace(',','.'))
            args[3] = self.parse_data(args[3])
            Pokupka(*tuple(args)) # data spisok pokupok is updated with this pokupka
            self.controller.save(self.data)
            self.unhighlight_error('product')
            self.unhighlight_error('price')
        except ValueError:
            self.highlight_error('price')
            return 0
            
        self.index = 0
        self.update_output()
        self.clear_input()
        #print self.data.sum
   
    def hideEvent(self, event):
        ''' event to handle hide '''
        self.hide()
        self.sysTray.setVisible(True)
        event.ignore()

    def __icon_activated(self, reason):
        ''' activate tray icon on hide '''
        if reason == QtGui.QSystemTrayIcon.DoubleClick:
            self.sysTray.setVisible(False)
            self.show()


